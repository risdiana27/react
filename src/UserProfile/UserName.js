import React from 'react';

const UserName = props => (
    <p>{props.username}</p>
);

UserName.defaultProps = {
    username: "User"
}

export default UserName;