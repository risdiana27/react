import React from 'react';
import Avatar from './Avatar';
import UserName from './UserName';
import UserName1 from './UserName1';
import Bio from './Bio';

const UserProfile = () => (
    <div>
        <Avatar />
        <UserName1 />
        {/* <UserName username="risdiana27" /> */}
        <Bio bio="Plus Ultra!" />
    </div>
   
);


export default UserProfile;