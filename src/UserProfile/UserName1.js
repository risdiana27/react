import React from "react";

class UserName1 extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            username: "Asep"
        }
    }

    render(){
        return(
           <p>{this.state.username}</p>
        )
    }
}

export default UserName1;